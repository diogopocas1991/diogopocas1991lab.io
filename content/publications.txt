Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4

====== blank ======

====== Publications ======

=== 2024 ===
* **Polymorphic Higher-order Context-free Session Types** [[[https://doi.org/10.1016/j.tcs.2024.114582|doi]]]
		With Diana Costa, Andreia Mordido and Vasco T. Vasconcelos.
		//Theoretical Computer Science//.

=== 2023 ===
* **Comparing the expressive power of Strongly-Typed and Grammar-Guided Genetic Programming** [[[https://dl.acm.org/doi/10.1145/3583131.3590507|doi]]] [[[./../files/papers/gecco2023.pdf|pdf]]]
		With Alcides Fonseca.
		//Genetic and Evolutionary Computation Conference (GECCO 2023)//.
* **A Unifying Approximate Potential for Weighted Congestion Games** [[[https://doi.org/10.1007/s00224-023-10133-z|doi]]] [[[https://arxiv.org/abs/2005.10101|arXiv]]]
		With Yiannis Giannakopoulos.
		//Theory of Computing Systems//.
* **System F^μ_ω with Context-free Session Types** [[[https://doi.org/10.1007/978-3-031-30044-8_15|doi]]] [[[https://arxiv.org/abs/2301.08659|arXiv]]]
		With Diana Costa, Andreia Mordido and Vasco T. Vasconcelos.
		//32nd European Symposium on Programming (ESOP 2023)//.
* **On the Complexity of Equilibrium Computation in First-Price Auctions** [[[https://doi.org/10.1137/21M1435823|doi]]] [[[https://arxiv.org/abs/2103.03238|arXiv]]]
		With Aris Filos-Ratsikas, Yiannis Giannakopoulos, Alexandros Hollender and Philip Lazos.
		//SIAM Journal on Computing//.
* **Robust Revenue Maximization Under Minimal Statistical Information** [[[https://doi.org/10.1145/3546606|doi]]] [[[https://arxiv.org/abs/1907.04220|arXiv]]]
		With Yiannis Giannakopoulos and Alexandros Tsigonias-Dimitriadis.
		//ACM Transactions on Economics and Computation//.

=== 2022 ===
* **Existence and complexity of approximate equilibria in weighted congestion games** [[[https://doi.org/10.1287/moor.2022.1272|doi]]] [[[https://arxiv.org/abs/2002.07466|arXiv]]]
		With George Christodoulou, Martin Gairing, Yiannis Giannakopoulos and Clara Waldmann.
		//Mathematics of Operations Research//.
* **Higher-order Context-free Session Types in System F** [[[https://doi.org/10.4204/EPTCS.356.3|doi]]]
		With Diana Costa, Andreia Mordido and Vasco T. Vasconcelos.
		//13th International Workshop on Programming Language Approaches to Concurrency and Communication-cEntric Software (PLACES 2022)//.
* **The Different Shades of Infinite Session Types** [[[https://doi.org/10.1007/978-3-030-99253-8_18|doi]]] [[[https://arxiv.org/abs/2201.08275|arXiv]]]
		With Simon J. Gay and Vasco T. Vasconcelos.
		//25th International Conference on Foundations of Software Science and Computation Structures (FoSSaCS 2022)//.
* **The Power of Machines That Control Experiments** [[[https://doi.org/10.1142/S0129054122500010|doi]]] [[[./../files/papers/ijfcs2022.pdf|pdf]]]
		With José Félix Costa and Vasco Boavida de Brito. 
		//International Journal of Foundations of Computer Science//.

=== 2021 ===
* **The Competitive Pickup and Delivery Orienteering Problem for Balancing Carsharing Systems** [[[https://doi.org/10.1287/trsc.2021.1041|doi]]] [[[./../files/papers/tscience2021.pdf|pdf]]]
		With Layla Martin, Stefan Minner and Andreas S. Schulz.
		//Transportation Science//.
* **On the Complexity of Equilibrium Computation in First-Price Auctions** [[[https://doi.org/10.1145/3465456.3467627|doi]]] [[[https://arxiv.org/abs/2103.03238|arXiv]]]
		With Aris Filos-Ratsikas, Yiannis Giannakopoulos, Alexandros Hollender and Philip Lazos.
		//22nd ACM Conference on Economics and Computation (EC 2021)//.
* **A new Lower Bound for Deterministic Truthful Scheduling** [[[https://doi.org/10.1007/s00453-021-00847-2|doi]]] [[[https://arxiv.org/abs/2005.10054|arXiv]]]
		With Yiannis Giannakopoulos and Alexander Hammerl.
		//Algorithmica//.
* **Optimal Pricing for MHR and λ-regular Distributions** [[[https://doi.org/10.1145/3434423|doi]]] [[[https://arxiv.org/abs/1810.00800|arXiv]]]
		With Yiannis Giannakopoulos and Keyu Zhu.
		//ACM Transactions on Economics and Computation (TEAC)//.
* **Tracking computability of GPAC-generable functions** [[[https://doi.org/10.1093/logcom/exaa081|doi]]] [[[./../files/papers/jlc2020.pdf|pdf]]]
		With Jeffery Zucker.
		//Journal of Logic and Computation//.

=== 2020 ===
* **Robust revenue maximization under minimal statistical information** [[[https://doi.org/10.1007/978-3-030-64946-3_13|doi]]] [[[https://arxiv.org/abs/1907.04220|arXiv]]]
		With Yiannis Giannakopoulos and Alexandros Tsigonias-Dimitriadis.
		//16th Conference on Web and Internet Economics (WINE 2020)//.
* **A unifying approximate potential for weighted congestion games** [[[https://doi.org/10.1007/978-3-030-57980-7_7|doi]]] [[[https://arxiv.org/abs/2005.10101|arXiv]]]
		With Yiannis Giannakopoulos.
		//13th Symposium on Algorithmic Game Theory (SAGT 2020)//.
* **A new lower bound for deterministic truthful scheduling** [[[https://doi.org/10.1007/978-3-030-57980-7_15|doi]]] [[[https://arxiv.org/abs/2005.10054|arXiv]]]
		With Yiannis Giannakopoulos and Alexander Hammerl.
		//13th Symposium on Algorithmic Game Theory (SAGT 2020)//.
* **Existence and complexity of approximate equilibria in weighted congestion games** [[[https://doi.org/10.4230/LIPIcs.ICALP.2020.32|doi]]] [[[https://arxiv.org/abs/2002.07466|arXiv]]] [[[https://www.youtube.com/watch?v=ytc-QpDtUBs|talk]]]
		With George Christodoulou, Martin Gairing, Yiannis Giannakopoulos and Clara Waldmann.
		//47th International Colloquium on Automata, Languages, and Programming (ICALP 2020)//.
		My talk is available on YouTube (click the above link).
* **Tracking computability of GPAC-generable functions** [[[https://doi.org/10.1007/978-3-030-36755-8_14|doi]]] [[[./../files/papers/lfcs2020.pdf|pdf]]]
		With Jeffery Zucker.
		//Logical Foundations of Computer Science (LFCS 2020)//.

=== 2019 ===
* **Register requirement minimization of fixed-depth pipelines for streaming data applications** [[[https://doi.org/10.1109/SOCC46988.2019.1570548393|doi]]] [[[./../files/papers/socc2019.pdf|pdf]]]
		With Thomas Goldbrunner, Nguyen Anh Vu Doan, Thomas Wild and Andreas Herkersdorf.
		//32nd IEEE International System-on-Chip Conference (SOCC 2019)//.
* **Approximability in the GPAC** [[[https://doi.org/10.23638/LMCS-15(3:24)2019|doi]]] [[[https://arxiv.org/abs/1801.07661|arXiv]]]
		With Jeffery Zucker.
		//Logical Methods in Computer Science//.

=== 2018 ===
* **Analog networks on function data streams** [[[https://doi.org/10.3233/COM-170077|doi]]] [[[./../files/papers/computability2018.pdf|pdf]]]
		With Jeffery Zucker.
		//Computability: The Journal of the Association CiE//.
* **Transient growth in stochastic Burgers flows** [[[https://doi.org/10.3934/dcdsb.2018052|doi]]] [[[./../files/papers/dcdsb2018.pdf|pdf]]]
		With Bartosz Protas.
		//Discrete and Continuous Dynamical Systems - Series B//.

=== 2017 ===
* **Analog computability with differential equations** [[[http://hdl.handle.net/11375/22593|link]]]
		PhD dissertation, McMaster University.
		Advisor: Jeffery Zucker.
* **Solving Smullyan Puzzles with Formal Systems** [[[https://doi.org/10.1007/s10516-017-9339-1|doi]]] [[[./../files/papers/axiom2017.pdf|pdf]]]
		With José Félix Costa.
		//Axiomathes//.

=== 2016 ===
* **An analogue-digital model of computation: Turing machines with physical oracles** [[[https://doi.org/10.1007/978-3-319-33924-5_4|doi]]] [[[./../files/papers/auc2017.pdf|pdf]]]
		With Tânia Ambaram, Edwin Beggs, José Félix Costa and John V. Tucker.
		//Advances in Unconventional Computing. Emergence, Complexity and Computation//, 22, Chapter IV.
* **Fixed point techniques in analog systems** [[[https://doi.org/10.1007/978-3-319-30379-6_63|doi]]] [[[./../files/papers/mcaamse2016.pdf|pdf]]]
		With Jeffery Zucker.
		//Mathematical and Computational Approaches in Advancing Modern Science and Engineering//.
* **Computations with oracles that measure vanishing quantities** [[[https://doi.org/10.1017/S0960129516000219|doi]]] [[[./../files/papers/mscs2016.pdf|pdf]]]
		With Edwin Beggs, José Félix Costa and John V. Tucker.
		//Mathematical Structures in Computer Science//.

=== 2014 ===
* **Simple reaction systems and their classifications** [[[https://doi.org/10.1142/S012905411440005X|doi]]] [[[./../files/papers/ijfcs2014b.pdf|pdf]]]
		With Luca Manzoni and Antonio E. Porreca.
		//International Journal of Foundations of Computer Science//.
* **An analogue-digital Church-Turing thesis** [[[https://doi.org/10.1142/S0129054114400012|doi]]] [[[./../files/papers/ijfcs2014a.pdf|pdf]]]
		With Edwin Beggs, José Félix Costa and John V. Tucker.
		//International Journal of Foundations of Computer Science//.

=== 2013 ===
* **Oracles that measure thresholds: The Turing machine and the broken balance** [[[https://doi.org/10.1093/logcom/ext047|doi]]] [[[./../files/papers/jlc2013.pdf|pdf]]]
		With Edwin Beggs, José Félix Costa and John V. Tucker.
		//Journal of Logic and Computation//.
* **On the power of threshold measurements as oracles** [[[https://doi.org/10.1007/978-3-642-39074-6_3|doi]]] [[[./../files/papers/lncs2013.pdf|pdf]]]
		With Edwin Beggs, José Félix Costa and John V. Tucker.
		//Unconventional Computation and Natural Computation//.
* **Complexity with costing and stochastic oracles** [[[https://fenix.tecnico.ulisboa.pt/downloadFile/2589874019364/comporac.pdf|link]]]
		MSc dissertation, Instituto Superior Técnico.
		Advisor: José Félix Costa.

=== 2012 ===
* **Testes de primalidade** [[[http://istpress.tecnico.ulisboa.pt/node/377|link]]] [[[./../files/papers/primal2012.pdf|pdf]]]
		//Números, cirurgias e nós de gravata: 10 anos de Seminário Diagonal no IST//, IST Press.